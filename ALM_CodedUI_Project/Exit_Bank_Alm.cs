﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    class Exit_Bank_Alm : WinWindow
    {

        public static void ExitBankAlm()
        {
            if (LibHelper.GetPropertyMenuItem("File", Open_Workspace_Calculate.almWindow) == "True")
            {
                //Click File            
                UITestControl fileMenu = new WinMenuItem(Open_Workspace_Calculate.almWindow);
                fileMenu.SearchProperties[UITestControl.PropertyNames.Name] = "File";
                fileMenu.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(fileMenu);

                //Click Exit Bank ALM
                UITestControl extBnkAlm = new WinMenuItem(Open_Workspace_Calculate.almWindow);
                extBnkAlm.SearchProperties[UITestControl.PropertyNames.Name] = "Exit Bank ALM";
                extBnkAlm.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(extBnkAlm);
            }
            else
            {
                Console.WriteLine("Exit has been done");
            }
        }
    }
}
