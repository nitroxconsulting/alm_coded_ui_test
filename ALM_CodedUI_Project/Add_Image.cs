﻿using System;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Drawing;
using System.Windows.Forms;

namespace ALM_CodedUI_Project
{
    class Add_Image:WinWindow
    {

        public static void VerifyAddedImage()
        {
            UITestControl menuBar = new WinMenuBar();
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";
            
            UITestControl Options = new WinMenuItem(menuBar);
            Options.SearchProperties[UITestControl.PropertyNames.Name] = "Options";
            Options.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Options);

            UITestControl Parameters = new WinMenuItem(menuBar);
            Parameters.SearchProperties[UITestControl.PropertyNames.Name] = "Parameters";
            Options.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Parameters);

            UITestControl ParameterWindow = new UITestControl();
            ParameterWindow.TechnologyName = "MSAA";
            ParameterWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Parameters";
            ParameterWindow.WindowTitles.Add("Parameters");

            UITestControl Browse_Image = new WinButton(ParameterWindow);
            Browse_Image.SearchProperties[UITestControl.PropertyNames.Name] = "Browse image ...";
            Browse_Image.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Browse_Image);
            
            UITestControl OpenWindow = new UITestControl();
            OpenWindow.TechnologyName = "MSAA";
            OpenWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Open";
            OpenWindow.SearchProperties[UITestControl.PropertyNames.ClassName] = "#32770";
            OpenWindow.WindowTitles.Add("Open");

            UITestControl SelectImage = new WinListItem(OpenWindow);
            SelectImage.SearchProperties[WinList.PropertyNames.Name] = "testpic.jpg";
            SelectImage.WindowTitles.Add("Open");
            Mouse.DoubleClick(SelectImage);

            UITestControl OKButton = new WinButton(ParameterWindow);
            OKButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            OKButton.WindowTitles.Add("Parameters");
            Mouse.Click(OKButton);

            //TODO ASSSERT

        } 
    }
}
