﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class ScenarioValidation
    {
        public static void VerifyScennarioValidation()
        {

            UITestControl warning = new UITestControl(Open_Workspace_Calculate.almWindow);
            warning.TechnologyName = "MSAA";
            warning.SearchProperties[UITestControl.PropertyNames.Name] = "Scenario Setup Validation Result";
            warning.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            warning.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));
            Console.WriteLine("Scenario Setup Validation Result: " + warning.Exists.ToString());
            LibHelper.AsssertAreEquals("False", warning.Exists.ToString());

        }
    }
}
