﻿using System;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Drawing;
using System.Windows.Forms;

namespace ALM_CodedUI_Project
{
    class FixedRate:WinWindow
    {
        public static void VerifyFixedRate()
        {
            UITestControl TreeViewWindow = new WinWindow();
            TreeViewWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";
            TreeViewWindow.WindowTitles.Add("ALM Solutions beta - Bank ALM - [Test Oli]");

            UITestControl Portfolio = new WinTreeItem(TreeViewWindow);
            Portfolio.SearchProperties[WinTreeItem.PropertyNames.Name] = "Portfolio      ";
            Portfolio.SearchProperties["Value"] = "0";
            Portfolio.WindowTitles.Add("ALM Solutions beta - Bank ALM - [Test Oli]");

            UITestControl BalanceSheet = new WinTreeItem(Portfolio);
            BalanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Balance Sheet      ";
            BalanceSheet.SearchProperties["Value"] = "1";
            BalanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            BalanceSheet.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            BalanceSheet.WindowTitles.Add("ALM Solutions beta - Bank ALM - [Test Oli]");

            UITestControl Asset = new WinTreeItem(BalanceSheet);
            Asset.SearchProperties[WinTreeItem.PropertyNames.Name] = "Asset      ";
            Asset.SearchProperties["Value"] = "2";
            Asset.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Asset.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Asset.WindowTitles.Add("ALM Solutions beta - Bank ALM - [Test Oli]");

            UITestControl Category = new WinTreeItem(Asset);
            Category.SearchProperties[WinTreeItem.PropertyNames.Name] = "Financial investments      ";
            Category.SearchProperties["Value"] = "3";
            Category.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Category.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Category.WindowTitles.Add("ALM Solutions beta - Bank ALM - [Test Oli]");
            Mouse.Click(Category);

            UITestControl balanceHeaderPanel = new WinControl();
            balanceHeaderPanel.SearchProperties[WinControl.PropertyNames.ControlName] = "balanceHeaderPanel";
            balanceHeaderPanel.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);

            UITestControl AddNewButton = new WinButton(balanceHeaderPanel);
            AddNewButton.SearchProperties[WinButton.PropertyNames.Name] = "Add New";
            Mouse.Click(AddNewButton);

            UITestControl AddProductWindow = new WinWindow();
            AddProductWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Add products";
            AddProductWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));
            AddProductWindow.WindowTitles.Add("Add products");

            UITestControl ComboBoxWindow = new WinWindow(AddProductWindow);
            ComboBoxWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "comboBoxBondFinancialType";
            ComboBoxWindow.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            ComboBoxWindow.WindowTitles.Add("Add products");

            WinComboBox FinancialType = new WinComboBox(ComboBoxWindow);
            FinancialType.SelectedItem = "Fixed Rate";

            UITestControl CurrencyPanel = new WinWindow();
            CurrencyPanel.SearchProperties[WinWindow.PropertyNames.ControlName] = "panel";
            CurrencyPanel.WindowTitles.Add("Add products");

            WinComboBox Currency = new WinComboBox(CurrencyPanel);
            Currency.SearchProperties[WinComboBox.PropertyNames.Name] = "Currency";
            Currency.SelectedItem = "USD";

            //FinancialType.SelectedItem = "Stock Equity";

            WinControl OKButton = new WinButton(AddProductWindow);
            OKButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            OKButton.WindowTitles.Add("Add products");
            Mouse.Click(OKButton);

            LibHelper.AsssertAreEquals("false", AddProductWindow.GetProperty("Exists").ToString());

        }
    }
}
