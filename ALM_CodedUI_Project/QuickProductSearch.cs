﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class QuickProductSearch
    {
        public static void VerifyProductSearch()
        {

            UITestControl mainFormWindow = new UITestControl(Open_Workspace_Calculate.almWindow);
            mainFormWindow.SearchProperties[UITestControl.PropertyNames.Name] = "MainForm";

            UITestControl itemWindow = new UITestControl(mainFormWindow);
            itemWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.EDIT", PropertyExpressionOperator.Contains));

            WinEdit itemEdit = new WinEdit(itemWindow);

            itemEdit.Text = "A00015";

            WinToolBar searchButton = new WinToolBar(itemWindow);
            searchButton.SearchProperties[WinToolBar.PropertyNames.Name] = "toolStrip1";

            WinButton actualButtonSearch = new WinButton(searchButton);
            actualButtonSearch.SearchProperties[WinButton.PropertyNames.Name] = "Search Product";

            Mouse.Click(actualButtonSearch);

            UITestControl gridWindow = new WinWindow(mainFormWindow);
            gridWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "detailsGrid";

            UITestControl gridViewTable = new WinTable(gridWindow);
            gridViewTable.SearchProperties[WinWindow.PropertyNames.Name] = "DataGridView";

            UITestControl row = new WinRow(gridViewTable);
            row.SearchProperties[WinRow.PropertyNames.Name] = "Row 2";

            LibHelper.AsssertAreEquals(true, row.Exists);
        }
    }
}
