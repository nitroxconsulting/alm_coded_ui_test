﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class Tools_Gap_Analysis
    {
        public static string rateval = "5%";
        public static string gasTypeVal = "Fixed Interest Rate";
        public static string cashFlowVal = "Treasury";

        public static void gapAnalysis()
        {
            openToolsMenu();
            UITestControl gapAnalyisWindow = new UITestControl();
            gapAnalyisWindow.TechnologyName = "MSAA";
            gapAnalyisWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Gap Analysis Parameters Editor";
            gapAnalyisWindow.WindowTitles.Add("Gap Analysis Parameters Editor");

            WinComboBox gapType = new WinComboBox(gapAnalyisWindow);
            gapType.SearchProperties[WinComboBox.PropertyNames.Name] = "Gap Type";

            WinEdit rate = new WinEdit(gapAnalyisWindow);
            rate.SearchProperties[WinComboBox.PropertyNames.Name] = "Actualisation factor";

            WinComboBox cashFlow = new WinComboBox(gapAnalyisWindow);
            cashFlow.SearchProperties[WinComboBox.PropertyNames.Name] = "Cashflow presentation";

            rate.Text = rateval;
            gapType.SelectedItem = gasTypeVal;
            cashFlow.SelectedItem = cashFlowVal;

            WinButton okButton = new WinButton(gapAnalyisWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "Ok";
            Mouse.Click(okButton);

            openToolsMenu();

            LibHelper.AsssertAreEquals(rateval, rate.Text.ToString());
            LibHelper.AsssertAreEquals(cashFlowVal, cashFlow.SelectedItem.ToString());
            LibHelper.AsssertAreEquals(gasTypeVal, gapType.SelectedItem.ToString());

            Mouse.Click(okButton);

        }

        public static void openToolsMenu()
        {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl taxAssumptionBut = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            taxAssumptionBut.SearchProperties[UITestControl.PropertyNames.Name] = "Gap Analysis";
            taxAssumptionBut.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(taxAssumptionBut);
        }
    }
}
