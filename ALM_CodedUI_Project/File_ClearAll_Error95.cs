﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ALM_CodedUI_Project
{
    class File_ClearAll_Error95 : WinWindow
    {

        public static string curName = "USD";
        public static string symbool = "$";
        public static string rate = "CDR_base";
        public static void MenuClearAll()
        {
            Mouse.Click(Open_Workspace_Calculate.fileMenu);

            Open_Workspace_Calculate.clearAll.SearchProperties[UITestControl.PropertyNames.Name] = "Clear All";
            Mouse.Click(Open_Workspace_Calculate.clearAll);

            UITestControl balanceSheet = new WinTreeItem(Open_Workspace_Calculate.almWindow);
            balanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Blance Sheet";
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);

            //Assertions
            LibHelper.AsssertAreEquals("False", LibHelper.GetPropertyWinTreeItem("Blance Sheet", Open_Workspace_Calculate.almWindow));
        }

        public static void ClearAllCurrency()
        {
            Add_Currency.openCurrency();

            UITestControl currencyWindow = new UITestControl();
            currencyWindow.TechnologyName = "MSAA";
            currencyWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Currencies";
            currencyWindow.WindowTitles.Add("Currencies");

            UITestControl addNewCurrency = new WinButton(currencyWindow);
            addNewCurrency.SearchProperties[UITestControl.PropertyNames.Name] = "Add new currency";
            addNewCurrency.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(addNewCurrency);

            UITestControl addCurWindow = new UITestControl();
            addCurWindow.TechnologyName = "MSAA";
            addCurWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Add currencies";
            addCurWindow.WindowTitles.Add("Add currencies");



            UITestControl NumberofProducts = new WinButton(addCurWindow);
            NumberofProducts.SearchProperties[UITestControl.PropertyNames.Name] = "OK";
            NumberofProducts.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(NumberofProducts);

            WinCell currencyName = new WinCell(currencyWindow);
            currencyName.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyName.Value = curName;

            WinCell currencySymbol = new WinCell(currencyWindow);
            currencySymbol.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencySymbol.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencySymbol.Value = symbool;

            WinCell currencyRate = new WinCell(currencyWindow);
            currencyRate.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyRate.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencyRate.Value = rate;

            UITestControl closeButton = new WinButton(currencyWindow);
            closeButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
            closeButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(closeButton);

            Mouse.Click(Open_Workspace_Calculate.fileMenu);

            Open_Workspace_Calculate.clearAll.SearchProperties[UITestControl.PropertyNames.Name] = "Clear All";
            Mouse.Click(Open_Workspace_Calculate.clearAll);

            SaveChanges2.saveChanges2();

            Mouse.Click(Open_Workspace_Calculate.fileMenu);

            UITestControl NewPortfolio = new WinMenuItem(Open_Workspace_Calculate.fileMenu);
       
            NewPortfolio.SearchProperties[UITestControl.PropertyNames.Name] = "New Portfolio";
            Mouse.Click(NewPortfolio);

            UITestControl Template = new WinMenuItem(NewPortfolio);
            Template.SearchProperties[UITestControl.PropertyNames.Name] = "With Asia Template";
            Mouse.Click(Template);

            //Open_Workspace_Calculate.OpenCalculateWorkspace(AlmMain.alw7, false, false);
        
            Add_Currency.openCurrency();

            //Access Outer Grid
            WinWindow variablesAdded = new WinWindow();
            variablesAdded.SearchProperties[WinWindow.PropertyNames.ControlName] = "currenciesGrid";

            //Table Data
            WinTable variablesTable = new WinTable(variablesAdded);
            variablesTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            //Actual Cell
            WinCell newAddedCell = new WinCell(variablesTable);
            newAddedCell.SearchProperties[WinCell.PropertyNames.Value] = curName;

            //Assert table Data
            LibHelper.AsssertAreEquals("False", newAddedCell.Exists.ToString());

            Mouse.Click(closeButton);

        }
    }
}
