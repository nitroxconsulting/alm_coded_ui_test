﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class OCI_BalanceSheet
    {
        public static string stockEquity = "Stock Equity";
        public static void checkOCI()
        {
            //Below are tree items please follow precedence base on variable decleration
            UITestControl main = new WinWindow();
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            //This is the Main Form Outer Form
            UITestControl treeView = new WinWindow(main);
            treeView.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";
        
            //Portfolio
            WinTreeItem portfolio = new WinTreeItem(treeView);
            portfolio.SearchProperties[WinTreeItem.PropertyNames.Name] = "Portfolio";
            portfolio.SearchProperties["Value"] = "0";

            //Balance Sheet
            WinTreeItem balanceSheet = new WinTreeItem(treeView);
            balanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Balance Sheet";
            balanceSheet.SearchProperties["Value"] = "1";
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            //Equity
            WinTreeItem equity = new WinTreeItem(treeView);
            equity.SearchProperties[WinTreeItem.PropertyNames.Name] = "Equity";
            equity.SearchProperties["Value"] = "2";
            equity.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            equity.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            //OCI
            WinTreeItem oci = new WinTreeItem(treeView);
            oci.SearchProperties[WinTreeItem.PropertyNames.Name] = "OCI";
            oci.SearchProperties["Value"] = "3";
            oci.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            oci.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.Click(oci);

            //Add New Button - Upper Right!
            WinButton addNew = new WinButton(main);
            addNew.SearchProperties[WinButton.PropertyNames.Name] = "Add New";
            Mouse.Click(addNew);

            //Add Products Windows & Select Financial Type
            UITestControl addProductWindow = new UITestControl();
            addProductWindow.TechnologyName = "MSAA";
            addProductWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Add products";
            addProductWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));
            addProductWindow.WindowTitles.Add("Add products");

            UITestControl financialType = new UITestControl(addProductWindow);
            financialType.SearchProperties[WinComboBox.PropertyNames.ControlName] = "comboBoxBondFinancialType";

            WinComboBox financialTypeCombo = new WinComboBox(financialType);
            financialTypeCombo.SelectedItem = stockEquity;

            //Number of Stocks
            WinComboBox numOfStocks = new WinComboBox(addProductWindow);
            numOfStocks.SearchProperties[WinComboBox.PropertyNames.Name] = "Number of stocks";
            numOfStocks.SelectedItem = "1";

            WinButton okButton = new WinButton(addProductWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            UITestControl grid = new UITestControl(main);
            grid.SearchProperties[WinWindow.PropertyNames.ControlName] = "detailsGrid";

            WinTable table = new WinTable(grid);
            table.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinRow row = new WinRow(table);
            row.SearchProperties[WinRow.PropertyNames.Name] = "Row 1";
            //row.SearchProperties[WinRow.PropertyNames.Value] = @"E00019;Stock Equity;(null);False;System.Drawing.Bitmap;(null);False;BULLET;0;(null);Constant;(null);AFS;1;100.00;100.00;100.00;No Roll;100.00 %;(null);133;(null);0.0000 %;Floating;In Advance;ACTACT;Monthly;Normal;In Balance;(null);(null);(null);(null);€;0;0.00;(null);Not eligible;0.000 %;0.000 %;(null);0.0000 %;0;0.0000 %;(null);(null);(null)";

            WinCell actuarialPrice = new WinCell(row);
            actuarialPrice.SearchProperties[WinCell.PropertyNames.Name] = "Actuarial Price Row 1";
            actuarialPrice.SearchProperties[WinCell.PropertyNames.Value] = "100.00";

            LibHelper.AsssertAreEquals("100.00", actuarialPrice.Value.ToString());
        }
    }
}
