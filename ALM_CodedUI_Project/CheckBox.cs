﻿using System;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ALM_CodedUI_Project
{
    class CheckBox:WinWindow
    {
        public static Boolean confirm = true;
        public static UITestControl ParameterWindow = new UITestControl();
        public static WinCheckBox CheckBoxes = new WinCheckBox(ParameterWindow);
        public static UITestControl menuBar = new WinMenuBar();
        public static UITestControl Options = new WinMenuItem(menuBar);
        public static UITestControl Parameters = new WinMenuItem(menuBar);
        public static UITestControl OKButton = new WinButton(ParameterWindow);

        public static void VerifyCheckBox()
        {
            CheckBoxChecked("Confirm before deleting a line");
            CheckBoxChecked("Import deletes missing products ");
            CheckBoxChecked("Link swaps 2 by 2 following order during import");
            
            //Options -> parameters
            Mouse.Click(Options);
            Mouse.Click(Parameters);

            //Assert Each Check Boxes
            CheckBoxes.SearchProperties[WinCheckBox.PropertyNames.Name] = "Confirm before deleting a line";
            LibHelper.AsssertAreEquals("True", CheckBoxes.GetProperty("Enabled").ToString());

            CheckBoxes.SearchProperties[WinCheckBox.PropertyNames.Name] = "Import deletes missing products ";
            LibHelper.AsssertAreEquals("True", CheckBoxes.GetProperty("Enabled").ToString());

            CheckBoxes.SearchProperties[WinCheckBox.PropertyNames.Name] = "Link swaps 2 by 2 following order during import";
            LibHelper.AsssertAreEquals("True", CheckBoxes.GetProperty("Enabled").ToString());

            Mouse.Click(OKButton);
        }

        public static void CheckBoxChecked(String name)
        {
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";


            Options.SearchProperties[UITestControl.PropertyNames.Name] = "Options";
            Options.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Options);


            Parameters.SearchProperties[UITestControl.PropertyNames.Name] = "Parameters";
            Options.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Parameters);

            ParameterWindow.TechnologyName = "MSAA";
            ParameterWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Parameters";
            ParameterWindow.WindowTitles.Add("Parameters");

            CheckBoxes.SearchProperties[WinCheckBox.PropertyNames.Name] = name;
            CheckBoxes.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(CheckBoxes);


            OKButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            OKButton.WindowTitles.Add("Parameters");
            Mouse.Click(OKButton);
        }
    }
}
