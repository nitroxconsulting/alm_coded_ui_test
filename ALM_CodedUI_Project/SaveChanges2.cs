﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class SaveChanges2 : WinWindow
    {
        public static void saveChanges2()
        {
            UITestControl unsavedChanges = new WinWindow();

            unsavedChanges.TechnologyName = "MSAA";
            unsavedChanges.SearchProperties[WinWindow.PropertyNames.Name] = "OK";
            unsavedChanges.SearchProperties[WinWindow.PropertyNames.ClassName] = "Button";

            WinButton a = new WinButton(unsavedChanges);
            a.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(a);
        }
    }
}
