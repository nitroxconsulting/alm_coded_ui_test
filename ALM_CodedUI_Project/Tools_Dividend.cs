﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class Tools_Dividend
    {
        public static string dividendName = "Sample Name";
        public static string value = "ois";
        public static void testDividend()
        {
            openToolsMenu();
            UITestControl dividendWindow = new UITestControl();
            dividendWindow.TechnologyName = "MSAA";
            dividendWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Dividend";
            dividendWindow.WindowTitles.Add("Dividend");

            WinButton add = new WinButton(dividendWindow);
            add.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            Mouse.Click(add);

            UITestControl noOfProducts = new UITestControl();
            noOfProducts.TechnologyName = "MSAA";
            noOfProducts.SearchProperties[UITestControl.PropertyNames.Name] = "Dividend";
            noOfProducts.WindowTitles.Add("Dividend");

            WinButton okButton = new WinButton(noOfProducts);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            WinCell name = new WinCell(dividendWindow);
            name.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            name.Value = dividendName;

            WinCell val = new WinCell(dividendWindow);
            val.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            val.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            val.Value = value;

            WinButton okButton2 = new WinButton(dividendWindow);
            okButton2.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton2);

            openToolsMenu();

            //Access Outer Grid
            WinWindow variablesAdded = new WinWindow();
            variablesAdded.SearchProperties[WinWindow.PropertyNames.ControlName] = "grid";

            //Table Data
            WinTable variablesTable = new WinTable(variablesAdded);
            variablesTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            //Actual Cell
            WinCell newAddedCell = new WinCell(variablesTable);
            newAddedCell.SearchProperties[WinCell.PropertyNames.Value] = dividendName;

            //Assert table Data
            LibHelper.AsssertAreEquals(dividendName, newAddedCell.Value);

            Mouse.Click(okButton);
        }

        public static void openToolsMenu() {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl dividend = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            dividend.SearchProperties[UITestControl.PropertyNames.Name] = "Dividend";
            dividend.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(dividend);
        }
    }
}
