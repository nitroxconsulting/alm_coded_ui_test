﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    class Tools_DynamicVariables
    {
        public static void testDynamicVariables()
        {
            openToolsMenu();

            UITestControl window = new UITestControl();
            window.TechnologyName = "MSAA";
            window.SearchProperties[WinWindow.PropertyNames.Name] = "Dynamic Variable Manager";
            window.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(window);

            UITestControl addButton = new WinButton(window);
            addButton.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            addButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);

           // for(int i=0; i <= 5; i++)
                Mouse.Click(addButton);

            UITestControl okButton = new WinButton(window);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            okButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(okButton);

            openToolsMenu();
            Mouse.Click(window);

            UITestControl variableList = new WinWindow(window);
            window.TechnologyName = "MSAA";
            window.SearchProperties[WinWindow.PropertyNames.ControlName] = "dynamicVarListBox";
            window.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);

            WinListItem uIDynamicVariable0ListItem = new WinListItem(variableList);
            uIDynamicVariable0ListItem.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable0";
           /* WinListItem uIDynamicVariable1ListItem = new WinListItem(variableList);
            uIDynamicVariable1ListItem.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable1";
            WinListItem uIDynamicVariable2ListItem = new WinListItem(variableList);
            uIDynamicVariable2ListItem.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable2";
            WinListItem uIDynamicVariable3ListItem = new WinListItem(variableList);
            uIDynamicVariable3ListItem.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable3";
            WinListItem uIDynamicVariable4ListItem = new WinListItem(variableList);
            uIDynamicVariable4ListItem.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable4";*/

            LibHelper.AsssertAreEquals(true, uIDynamicVariable0ListItem.Exists);
           /* LibHelper.AsssertAreEquals(true, uIDynamicVariable1ListItem.Exists);
            LibHelper.AsssertAreEquals(true, uIDynamicVariable2ListItem.Exists);
            LibHelper.AsssertAreEquals(true, uIDynamicVariable3ListItem.Exists);
            LibHelper.AsssertAreEquals(true, uIDynamicVariable4ListItem.Exists);*/

            UITestControl delBut = new WinWindow(window);
            delBut.SearchProperties[WinWindow.PropertyNames.ControlName] = "deleteButton";

            UITestControl deleteButton = new WinButton(delBut);
            deleteButton.SearchProperties[WinButton.PropertyNames.Name] = "Delete";
            deleteButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);

            //for (int i = 0; i <= 5; i++)
                Mouse.Click(deleteButton);

            Mouse.Click(okButton);

            openToolsMenu();
            Mouse.Click(window);

            LibHelper.AsssertAreEquals(false, uIDynamicVariable0ListItem.Exists);
            /*LibHelper.AsssertAreEquals(false, uIDynamicVariable1ListItem.Exists);
            LibHelper.AsssertAreEquals(false, uIDynamicVariable2ListItem.Exists);
            LibHelper.AsssertAreEquals(false, uIDynamicVariable3ListItem.Exists);
            LibHelper.AsssertAreEquals(false, uIDynamicVariable4ListItem.Exists);*/

            UITestControl cancelButton = new WinButton(window);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "Cancel";
            okButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(cancelButton);

            Mouse.Click(Open_Workspace_Calculate.almWindow);

        }
        public static void openToolsMenu()
        {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl taxAssumptionBut = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            taxAssumptionBut.SearchProperties[UITestControl.PropertyNames.Name] = "Dynamic Variables...";
            taxAssumptionBut.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(taxAssumptionBut);
        }
    }
}
