﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ALM_CodedUI_Project
{
    class AddRollSpecValue:WinWindow
    {
        public static UITestControl DataGridView = new WinWindow();
        public static UITestControl DataGridViewTable = new WinTable(DataGridView);
        public static UITestControl RollSpecRow = new WinRow(DataGridViewTable);
        public static String value = "R00001";
        public static void VerifyAddRollSpecValue()
        {
            UITestControl TreeViewWindow = new WinWindow();
            TreeViewWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";

            UITestControl Portfolio = new WinTreeItem(TreeViewWindow);
            Portfolio.SearchProperties[WinTreeItem.PropertyNames.Name] = "Portfolio";
            Portfolio.SearchProperties["Value"] = "0";

            UITestControl BalanceSheet = new WinTreeItem(Portfolio);
            BalanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Balance Sheet";
            BalanceSheet.SearchProperties["Value"] = "1";
            BalanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            BalanceSheet.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            UITestControl Asset = new WinTreeItem(BalanceSheet);
            Asset.SearchProperties[WinTreeItem.PropertyNames.Name] = "Asset";
            Asset.SearchProperties["Value"] = "2";
            Asset.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Asset.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            UITestControl Category = new WinTreeItem(Asset);
            Category.SearchProperties[WinTreeItem.PropertyNames.Name] = "Financial investments";
            Category.SearchProperties["Value"] = "3";
            Category.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Category.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.Click(Category);

            SelectRollSpecCell();
            
            WinCell RollSpec = new WinCell(RollSpecRow);
            RollSpec.SearchProperties[WinCell.PropertyNames.Value] = "No Roll";
            Mouse.Click(RollSpec);

            UITestControl AddNew = new WinListItem(RollSpec);
            AddNew.SearchProperties[WinListItem.PropertyNames.Name] = "Create New";
            Mouse.Click(AddNew);

            WinComboBox ComboBox = new WinComboBox(RollSpecRow);
            ComboBox.SearchProperties[WinComboBox.PropertyNames.Name] = "Editing Control";

            LibHelper.AsssertAreEquals(value.ToString(), ComboBox.SelectedItem.ToString());
        }
        public static void SelectRollSpecCell()
        {
            DataGridView.SearchProperties[WinWindow.PropertyNames.ControlName] = "detailsGrid";
            
            DataGridViewTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";
            
            RollSpecRow.SearchProperties[WinRow.PropertyNames.Value] = @"A00006;Fixed Rate;(null);False;System.Drawing.Bitmap;(null);False;CST PAY;0;0;Constant;(null);L&R;100,000;100.00;100.00;100,000.00;No Roll;100.00 %;(null);6;(null);12%;Fixed;In Advance;A30360;Monthly;Normal;In Balance;(null);(null);(null);(null);$;0;0.00;(null);Not eligible;0.000 %;0.000 %;(null);0.0000 %;0;(null);(null);(null);(null)";
            RollSpecRow.SearchConfigurations.Add(SearchConfiguration.AlwaysSearch);
        }
    }
}
