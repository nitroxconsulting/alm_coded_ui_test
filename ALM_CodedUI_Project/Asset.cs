﻿using System;
using System.Threading;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using MouseButtons = System.Windows.Forms.MouseButtons;

namespace ALM_CodedUI_Project
{
    class Asset:WinWindow
    {
        public static String category = "newItem";
        public static void addAsset()
        {
            //Below are tree items please follow precedence base on variable decleration
            UITestControl main = new WinWindow();
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            //This is the Main Form Outer Form
            UITestControl treeView = new WinWindow(main);
            treeView.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";

            //Portfolio
            WinTreeItem portfolio = new WinTreeItem(treeView);
            portfolio.SearchProperties[WinTreeItem.PropertyNames.Name] = "Portfolio";
            portfolio.SearchProperties["Value"] = "0";

            //Balance Sheet
            WinTreeItem balanceSheet = new WinTreeItem(treeView);
            balanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Balance Sheet";
            balanceSheet.SearchProperties["Value"] = "1";
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            //Asset
            WinTreeItem asset = new WinTreeItem(treeView);
            asset.SearchProperties[WinTreeItem.PropertyNames.Name] = "Assets";
            asset.SearchProperties["Value"] = "2";
            asset.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            asset.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.Click(asset, MouseButtons.Right);

            WinWindow DropDownWindow = new WinWindow();
            DropDownWindow.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "DropDown";
            DropDownWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            UITestControl menu = new WinMenu(DropDownWindow);
            menu.TechnologyName = "MSAA";
            menu.SearchProperties[WinMenu.PropertyNames.Name] = "DropDown";

            WinMenuItem AddSubCategory = new WinMenuItem(menu);
            AddSubCategory.SearchProperties[WinMenuItem.PropertyNames.Name] = "Add a subcategory";
            Mouse.Click(AddSubCategory);

            //INPUT
            Keyboard.SendKeys(category + "{Enter}");

            WinTreeItem addedCategory = new WinTreeItem(treeView);
            addedCategory.SearchProperties[WinTreeItem.PropertyNames.Name] = category;
            addedCategory.SearchProperties["Value"] = "3";
            addedCategory.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            addedCategory.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            LibHelper.AsssertAreEquals("True", addedCategory.Exists.ToString());

            //DELETING
            Mouse.Click(addedCategory, MouseButtons.Right);
            WinMenuItem DeleteSubCategory = new WinMenuItem(menu);
            DeleteSubCategory.SearchProperties[WinMenuItem.PropertyNames.Name] = "Delete";
            Mouse.Click(DeleteSubCategory);

            LibHelper.AsssertAreEquals("False", addedCategory.Exists.ToString());
        }

        public static void copyPasteAsset()
        {
            //Below are tree items please follow precedence base on variable decleration
            UITestControl main = new WinWindow();
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            //This is the Main Form Outer Form
            UITestControl treeView = new WinWindow(main);
            treeView.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";

            //Placements and advances to credit institutions
            WinTreeItem firstAsset = new WinTreeItem(treeView);
            firstAsset.SearchProperties[WinTreeItem.PropertyNames.Name] = "Placements and advances to credit institutions";
            firstAsset.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            firstAsset.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.Click(firstAsset);

            //Data Grid View
            WinTable dataGridView = new WinTable(main);
            dataGridView.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            //Click the first row header
            WinRowHeader rowHeaderStart = new WinRowHeader(dataGridView);
            rowHeaderStart.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 0";
            Mouse.Click(rowHeaderStart);

            //Click the third row header while shifting
            WinRowHeader rowHeaderEnd = new WinRowHeader(dataGridView);
            rowHeaderEnd.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 2";
            Mouse.Click(rowHeaderEnd, ModifierKeys.Shift);
            //Right Click
            Mouse.Click(rowHeaderEnd, MouseButtons.Right);

            //Dropdown Window
            WinWindow DropDownWindow = new WinWindow();
            DropDownWindow.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "DropDown";
            DropDownWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            //Clicking the copy
            WinMenuItem copyMenuItem = new WinMenuItem(DropDownWindow);
            copyMenuItem.SearchProperties[WinMenuItem.PropertyNames.Name] = "Copy Line";
            Mouse.Click(copyMenuItem);


            //Debt Securities
            WinTreeItem debtSecurities = new WinTreeItem(treeView);
            debtSecurities.SearchProperties[WinTreeItem.PropertyNames.Name] = "Debt securities";
            debtSecurities.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            debtSecurities.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.DoubleClick(debtSecurities);
            
            //Debt Securities Child
            WinTreeItem debtSecurityChild = new WinTreeItem(treeView);
            debtSecurityChild.SearchProperties[WinTreeItem.PropertyNames.Name] = "at fair value through P&L";
            debtSecurityChild.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            debtSecurityChild.SearchConfigurations.Add(SearchConfiguration.NextSibling);
            Mouse.Click(debtSecurityChild);
            
            //Right click the first row header
            WinRowHeader rowHeader1 = new WinRowHeader(dataGridView);
            rowHeader1.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 0";
            Mouse.Click(rowHeader1, MouseButtons.Right);

            //Dropdown Window
            WinWindow DropDownWindow2 = new WinWindow();
            DropDownWindow2.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "DropDown";
            DropDownWindow2.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            //Clicking the paste
            WinMenuItem pasteMenuItem = new WinMenuItem(DropDownWindow2);
            pasteMenuItem.SearchProperties[WinMenuItem.PropertyNames.Name] = "Paste Line(s)";
            Mouse.Click(pasteMenuItem);

            //Assert
            WinRowHeader pastedRowHeader1 = new WinRowHeader(dataGridView);
            WinRowHeader pastedRowHeader2 = new WinRowHeader(dataGridView);
            WinRowHeader pastedRowHeader3 = new WinRowHeader(dataGridView);
            pastedRowHeader1.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 1";
            pastedRowHeader2.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 2";
            pastedRowHeader3.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 3";

            LibHelper.AsssertAreEquals("True", pastedRowHeader1.TryFind().ToString());
            LibHelper.AsssertAreEquals("True", pastedRowHeader2.TryFind().ToString());
            LibHelper.AsssertAreEquals("True", pastedRowHeader3.TryFind().ToString());
        }
    }
}
