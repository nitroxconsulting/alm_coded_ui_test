﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ALM_CodedUI_Project
{
    public class LibHelper
    {
        //This is created due to if Assert fails I will just give console output and program still continue to test
        public static void AsssertAreEquals(string expected, string actual)
        {
           try {
               Assert.AreEqual(expected, actual, "The expected data is not equal to the actual");
           }
           catch (AssertFailedException e)
           {
              Console.WriteLine("[Lib Helper]Exception "+e.Message);
           }
        }

        public static void AsssertAreEquals(bool expected, bool actual)
        {
            try
            {
                Assert.AreEqual(expected, actual, "The expected data is not equal to the actual");
            }
            catch (AssertFailedException e)
            {
                Console.WriteLine("[Lib Helper]Exception " + e.Message);
            }
        }

        //Getting the Property "Exists" and convert to string
        public static string GetPropertyWinTreeItem(string propertyName, UITestControl window) {

            string result = null;
            try {
                UITestControl balanceSheet = new WinTreeItem(window);
                balanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = propertyName;
                balanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                result = balanceSheet.GetProperty("Exists").ToString();
                return result;
            }
            catch (UITestControlNotFoundException e) {
                Console.WriteLine("[LibHelper: WinTreeItem]: "+e.Message);
                return "";
            }

        }

        public static string GetPropertyMenuItem(string propertyName, UITestControl window)
        {

            string result = null;
            try
            {
                UITestControl prop = new WinMenuItem(window);
                prop.SearchProperties[WinTreeItem.PropertyNames.Name] = propertyName;
                prop.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                result = prop.GetProperty("Exists").ToString();
                return result;
            }
            catch (UITestControlNotFoundException e)
            {
                Console.WriteLine("[LibHelper: WinTreeItem]: " + e.Message);
                return "";
            }

        }
    }
}
