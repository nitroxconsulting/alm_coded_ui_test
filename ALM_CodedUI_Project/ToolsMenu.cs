﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALM_CodedUI_Project
{
    class ToolsMenu
    {
        public static UITestControl main = new WinWindow();
        public static UITestControl dynamicVariableManager = new WinWindow();

        public static void runToolsMenu()
        {
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";
            dynamicVariableManager.SearchProperties[WinWindow.PropertyNames.ControlName] = "DynamicVariableManager";
            dynamicVariableManager.SearchProperties[WinWindow.PropertyNames.Name] = "Dynamic Variable Manager";

            addDynamicVariables(main, dynamicVariableManager);
            checkIfListItemExists("True", main, dynamicVariableManager);
            deleteDynamicVariables(main, dynamicVariableManager);
            checkIfListItemExists("False", main, dynamicVariableManager);
        }

        public static void addDynamicVariables(UITestControl main, UITestControl dvm)
        {
            openDynamicVariables(main);
            WinButton add = new WinButton(dvm);
            add.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            Mouse.Click(add);
            Mouse.Click(add);
            Mouse.Click(add);
            Mouse.Click(add);
            Mouse.Click(add);

            WinButton ok = new WinButton(dvm);
            ok.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(ok);
        }

        public static void deleteDynamicVariables(UITestControl main, UITestControl dvm)
        {
            openDynamicVariables(main);

            WinButton delete = new WinButton(dvm);
            delete.SearchProperties[WinButton.PropertyNames.Name] = "Delete";
            Mouse.Click(delete);
            Mouse.Click(delete);
            Mouse.Click(delete);
            Mouse.Click(delete);
            Mouse.Click(delete);

            WinButton ok = new WinButton(dvm);
            ok.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(ok);
        }

        public static void checkIfListItemExists(String result, UITestControl main, UITestControl dvm)
        {
            String check = "False";
            openDynamicVariables(main);

            WinListItem list0 = new WinListItem(dvm);
            list0.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable0";
            WinListItem list1 = new WinListItem(dvm);
            list1.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable1";
            WinListItem list2 = new WinListItem(dvm);
            list2.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable2";
            WinListItem list3 = new WinListItem(dvm);
            list3.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable3";
            WinListItem list4 = new WinListItem(dvm);
            list4.SearchProperties[WinListItem.PropertyNames.Name] = "DynamicVariable4";

            if(list0.TryFind() && list1.TryFind() && list2.TryFind() && list3.TryFind() && list4.TryFind())
            {
                check = "True";
            }

            LibHelper.AsssertAreEquals(result, check);
            
            WinButton cancel = new WinButton(dvm);
            cancel.SearchProperties[WinButton.PropertyNames.Name] = "Cancel";
            Mouse.Click(cancel);
        }

        public static void openDynamicVariables(UITestControl main)
        {
            WinMenuItem tools = new WinMenuItem(main);
            tools.SearchProperties[WinMenuItem.PropertyNames.Name] = "Tools";
            Mouse.Click(tools);

            WinMenuItem dynamicVar = new WinMenuItem(tools);
            dynamicVar.SearchProperties[WinMenuItem.PropertyNames.Name] = "Dynamic Variables...";
            Mouse.Click(dynamicVar);
        }
    }
}
