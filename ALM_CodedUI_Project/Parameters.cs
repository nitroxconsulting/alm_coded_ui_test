﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class Parameters : WinWindow
    {

        public static void checkParametersIfEmpty()
        {
            string check = "False";
            UITestControl parametersTab = new WinTabPage(Open_Workspace_Calculate.almWindow);
            parametersTab.SearchProperties[WinTabPage.PropertyNames.Name] = "Parameters";
            Mouse.Click(parametersTab);

            UITestControl importantMarketData = new WinGroup(Open_Workspace_Calculate.almWindow),
                scenariosSetup = new WinGroup(Open_Workspace_Calculate.almWindow);

            UITestControl IMDrow = new WinRow(importantMarketData),
                SSrow = new WinRow(scenariosSetup);

            IMDrow.SearchProperties[WinRow.PropertyNames.Name] = "Row 0";
            SSrow.SearchProperties[WinRow.PropertyNames.Name] = "Row 0";

            if (IMDrow.TryFind() || SSrow.TryFind())
            {
                check = "True";
            }

            LibHelper.AsssertAreEquals("True", check);
        }
    }
}
