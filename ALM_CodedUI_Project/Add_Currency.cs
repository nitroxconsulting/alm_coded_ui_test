﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Threading;

namespace ALM_CodedUI_Project
{
    public class Add_Currency : WinWindow
    {
        public static string curName = "USD";
        public static string symbool = "$";
        public static string rate = "123";
        public static void VerifyAddedCurrency()
        {
            openCurrency();

            UITestControl currencyWindow = new UITestControl();
            currencyWindow.TechnologyName = "MSAA";
            currencyWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Currencies";
            currencyWindow.WindowTitles.Add("Currencies");

            UITestControl addNewCurrency = new WinButton(currencyWindow);
            addNewCurrency.SearchProperties[UITestControl.PropertyNames.Name] = "Add new currency";
            addNewCurrency.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(addNewCurrency);

            UITestControl addCurWindow = new UITestControl();
            addCurWindow.TechnologyName = "MSAA";
            addCurWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Add currencies";
            addCurWindow.WindowTitles.Add("Add currencies");



            UITestControl NumberofProducts = new WinButton(addCurWindow);
            NumberofProducts.SearchProperties[UITestControl.PropertyNames.Name] = "OK";
            NumberofProducts.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(NumberofProducts);

            WinCell currencyName = new WinCell(currencyWindow);
            currencyName.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyName.Value = curName;

            WinCell currencySymbol = new WinCell(currencyWindow);
            currencySymbol.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencySymbol.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencySymbol.Value = symbool;

            WinCell currencyRate = new WinCell(currencyWindow);
            currencyRate.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyRate.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencyRate.Value = rate;

            UITestControl closeButton = new WinButton(currencyWindow);
            closeButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
            closeButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(closeButton);

            openCurrency();

            //Access Outer Grid
            WinWindow variablesAdded = new WinWindow();
            variablesAdded.SearchProperties[WinWindow.PropertyNames.ControlName] = "currenciesGrid";

            //Table Data
            WinTable variablesTable = new WinTable(variablesAdded);
            variablesTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            //Actual Cell
            WinCell newAddedCell = new WinCell(variablesTable);
            newAddedCell.SearchProperties[WinCell.PropertyNames.Value] = curName;

            //Assert table Data
            LibHelper.AsssertAreEquals(curName, newAddedCell.Value);

            Mouse.Click(closeButton);

        }

        public static void VerifyDeleteCurrency() {
            openCurrency();

            UITestControl currencyWindow = new UITestControl();
            currencyWindow.TechnologyName = "MSAA";
            currencyWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Currencies";
            currencyWindow.WindowTitles.Add("Currencies");

            UITestControl addNewCurrency = new WinButton(currencyWindow);
            addNewCurrency.SearchProperties[UITestControl.PropertyNames.Name] = "Add new currency";
            addNewCurrency.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(addNewCurrency);

            UITestControl addCurWindow = new UITestControl();
            addCurWindow.TechnologyName = "MSAA";
            addCurWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Add currencies";
            addCurWindow.WindowTitles.Add("Add currencies");



            UITestControl NumberofProducts = new WinButton(addCurWindow);
            NumberofProducts.SearchProperties[UITestControl.PropertyNames.Name] = "OK";
            NumberofProducts.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(NumberofProducts);

            WinCell currencyName = new WinCell(currencyWindow);
            currencyName.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyName.Value = curName;

            WinCell currencySymbol = new WinCell(currencyWindow);
            currencySymbol.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencySymbol.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencySymbol.Value = symbool;

            WinCell currencyRate = new WinCell(currencyWindow);
            currencyRate.SearchProperties[WinCell.PropertyNames.Value] = "(null)";
            currencyRate.SearchProperties[WinCell.PropertyNames.Instance] = "2";
            currencyRate.Value = rate;


            //Access Outer Grid
            WinWindow variablesAdded = new WinWindow();
            variablesAdded.SearchProperties[WinWindow.PropertyNames.ControlName] = "currenciesGrid";

            //Table Data
            WinTable variablesTable = new WinTable(variablesAdded);
            variablesTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinRowHeader currencyRow = new WinRowHeader(variablesTable);
            currencyRow.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 2";
            Mouse.Click(currencyRow);


            UITestControl deleteCurrency = new WinButton(currencyWindow);
            deleteCurrency.SearchProperties[UITestControl.PropertyNames.Name] = "Delete selected currencies";
            deleteCurrency.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(deleteCurrency);

            UITestControl DelCurrencyWindow = new UITestControl();
            DelCurrencyWindow.TechnologyName = "MSAA";
            DelCurrencyWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Delete Currency";

            UITestControl DelCurrencyYesButton = new WinButton(DelCurrencyWindow);
            DelCurrencyYesButton.SearchProperties[WinButton.PropertyNames.Name] = "Yes";
            Mouse.Click(DelCurrencyYesButton);
            
            LibHelper.AsssertAreEquals("False", currencyRow.TryFind().ToString());


            UITestControl closeButton = new WinButton(currencyWindow);
            closeButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
            closeButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(closeButton);
        }

        public static void openCurrency()
        {
            UITestControl menuBar = new WinMenuBar();
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";

            UITestControl Options = new WinMenuItem(menuBar);
            Options.SearchProperties[UITestControl.PropertyNames.Name] = "Options";
            Options.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(Options);

            UITestControl currenciesSubMenu = new WinMenuItem(menuBar);
            currenciesSubMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Currencies";
            currenciesSubMenu.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(currenciesSubMenu);
        }
    }
}

