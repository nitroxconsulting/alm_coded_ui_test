﻿using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ALM_CodedUI_Project
{
    public class Vector
    {
        public static UITestControl vectorManager = new WinWindow();
        public static WinButton newButton = new WinButton(vectorManager);
        public static UITestControl vectorEditorWindow = new WinWindow();
        public static WinEdit name = new WinEdit(vectorEditorWindow);
        public static WinWindow freqComboBoxWindow = new WinWindow(vectorEditorWindow);
        public static WinComboBox freq = new WinComboBox(freqComboBoxWindow);
        public static WinWindow typeComboBoxWindow = new WinWindow(vectorEditorWindow);
        public static WinComboBox type = new WinComboBox(typeComboBoxWindow);
        public static WinWindow marketDataWindow = new WinWindow(vectorEditorWindow);
        public static WinCheckBox importantMarketData = new WinCheckBox(marketDataWindow);
        public static WinWindow dataGridView = new WinWindow(vectorEditorWindow);
        public static WinTable tableGridView = new WinTable(dataGridView);
        public static UITestControl mainForm = new WinWindow(Open_Workspace_Calculate.almWindow);
        public static WinTabPage parameter = new WinTabPage(mainForm);
        public static WinWindow marketDataImportConfigWindow = new WinWindow();
        public static void createVector()
        {
            openToolsMenuVector();
         
            vectorManager.SearchProperties[WinWindow.PropertyNames.Name] = "Vector Manager";
           
            newButton.SearchProperties[WinButton.PropertyNames.Name] = "New...";
            Mouse.Click(newButton);
           
            vectorEditorWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Vector Editor";

            name.SearchProperties[WinEdit.PropertyNames.Name] = "Name :";
            name.Text = "vector_E3M";
         
            freqComboBoxWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "frequencyComboBox";

            freq.SelectedItem = "Monthly";

            typeComboBoxWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "typeComboBox";

            type.SelectedItem = "Percentage";

            typeComboBoxWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "importantCheckBox";

            Mouse.Click(importantMarketData);

            dataGridView.SearchProperties[WinWindow.PropertyNames.ControlName] = "valuesDataGridView";

            tableGridView.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinCell cell = new WinCell(tableGridView);
            cell.SearchProperties[WinCell.PropertyNames.Value] = "07/2016";

            Mouse.Click(cell, MouseButtons.Right);

            WinWindow itemWindow = new WinWindow();
            itemWindow.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "DropDown";
            itemWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            WinMenu dropDownMenu = new WinMenu(itemWindow);
            dropDownMenu.SearchProperties[WinMenu.PropertyNames.Name] = "DropDown";

            WinMenuItem peroid = new WinMenuItem(dropDownMenu);
            peroid.SearchProperties[WinMenuItem.PropertyNames.Name] = "Copy Value to All Cells Below with Increment";
            Mouse.Click(peroid);

            WinWindow incrementWindow = new WinWindow();
            incrementWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Increment Values";

            WinWindow incrementTextBox = new WinWindow(incrementWindow);
            incrementTextBox.SearchProperties[WinWindow.PropertyNames.ControlName] = "incrementTextBox";

            WinComboBox incrementValue = new WinComboBox(incrementTextBox);
            incrementValue.EditableItem = "0.009";

            WinButton okButton = new WinButton(incrementWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            WinButton okButtonVectorEditor = new WinButton(vectorEditorWindow);
            okButtonVectorEditor.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButtonVectorEditor);

            WinButton closeButton = new WinButton(vectorManager);
            closeButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
            Mouse.Click(closeButton);

            mainForm.SearchProperties[WinWindow.PropertyNames.Name] = "MainForm";
            parameter.SearchProperties[WinTabPage.PropertyNames.Name] = "Parameters";
            Mouse.Click(parameter);

            WinWindow marketParameterWindow = new WinWindow(mainForm);
            marketParameterWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "marketParametersDataGridView";

            WinTable dataGridTable = new WinTable(marketParameterWindow);
            dataGridTable.SearchProperties[WinWindow.PropertyNames.Name] = "DataGridView";

            WinRowHeader vectorE3M = new WinRowHeader(dataGridTable);

            LibHelper.AsssertAreEquals(true, vectorE3M.Exists);

            WinButton importMarketData = new WinButton(mainForm);
            importMarketData.SearchProperties[WinButton.PropertyNames.Name] = "Import Market Data";
            Mouse.Click(importMarketData);

            marketDataImportConfigWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Market Data Import Configuration";

            WinButton okButtonMarketDataImportConfig = new WinButton(marketDataImportConfigWindow);
            okButtonMarketDataImportConfig.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButtonMarketDataImportConfig);

            WinWindow openWindow = new WinWindow();
            openWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Open";

            WinListItem module4Doc = new WinListItem(openWindow);
            module4Doc.SearchProperties[WinListItem.PropertyNames.Name] = "module 4 - Excel file to import vector data – prefilled for module 4.xlsx";
            Mouse.DoubleClick(module4Doc);

            WinText vectorAddedText = new WinText();
            vectorAddedText.SearchProperties[WinText.PropertyNames.Name] = "1 vector was successfully imported in your scenario.";

            LibHelper.AsssertAreEquals(true, vectorAddedText.Exists);

            WinWindow okWindow = new WinWindow();
            okWindow.SearchProperties[WinWindow.PropertyNames.Name] = "OK";
            okWindow.SearchProperties[WinWindow.PropertyNames.ClassName] = "Button";

            WinButton vectorSuccessOkButton = new WinButton(okWindow);
            vectorSuccessOkButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(vectorSuccessOkButton);
        }

        public static void openToolsMenuVector()
        {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl staticVector = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            staticVector.SearchProperties[UITestControl.PropertyNames.Name] = "Static Vectors...";
            staticVector.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(staticVector);
        }
    }
}
