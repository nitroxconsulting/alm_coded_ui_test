﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class Open_Workspace_Calculate : WinWindow
    {
        //Instance of Windows Form
        public static UITestControl almWindow = new UITestControl();
        public static UITestControl almTitleBar = new WinTitleBar(almWindow);
        public static UITestControl menuItemModules = new WinMenuItem(almWindow);
        public static UITestControl menuItem = new WinMenuItem(almWindow);
        public static UITestControl clearAll = new WinMenuItem(almWindow);
        public static UITestControl saveWorkspace = new WinMenuItem(almWindow);

        //Instance of Menu Buttons [File, Edit, Tools, Options, Help]
        public static UITestControl fileMenu = new WinMenuItem(almWindow);
        public static UITestControl openWorkspace = new WinMenuItem(almWindow);
        public static UITestControl optionsMenu = new WinMenuItem(almWindow);
        public static UITestControl toolsMenu = new WinMenuItem(almWindow);

        public static UITestControl openingFileWindow = new UITestControl();
        public static UITestControl listItem = new WinListItem(openingFileWindow);
        public static UITestControl calculate = new WinButton(almWindow);

        public static void OpenCalculateWorkspace(string alwFileName, Boolean calculateWorkspace, Boolean saveChanges)
        {
            openBlankWorkSpace();

            //Click File -> Open Workspace            
            fileMenu.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            fileMenu.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(fileMenu);

            openWorkspace.SearchProperties[UITestControl.PropertyNames.Name] = "Open Workspace ...";
            openWorkspace.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(openWorkspace);

            if (saveChanges)
            {
                SaveChanges2.saveChanges2();
            }

            //Opened new Instance Window for Selecting a upload File
                openingFileWindow.TechnologyName = "MSAA";
                openingFileWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Open";
                openingFileWindow.SearchProperties[UITestControl.PropertyNames.ClassName] = "#32770";

                listItem.SearchProperties[UITestControl.PropertyNames.Name] = alwFileName;
                Mouse.DoubleClick(listItem);

            if (calculateWorkspace)
            {
                //Calculate
                calculate.SearchProperties[UITestControl.PropertyNames.Name] = "Calculate";
                calculate.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(calculate);
                Thread.Sleep(3000);
            }
        }

        public static void ImportScenario(string sctFileName, Boolean saveChanges)
        {
            openBlankWorkSpace();

            //Click File -> Open Workspace          
            fileMenu.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            fileMenu.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(fileMenu);

            openWorkspace.SearchProperties[UITestControl.PropertyNames.Name] = "Import Scenario";
            openWorkspace.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(openWorkspace);

            if (saveChanges)
            {
                SaveChanges2.saveChanges2();
            }

            //Opened new Instance Window for Selecting a upload File
            openingFileWindow.TechnologyName = "MSAA";
            openingFileWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Open";

            listItem.SearchProperties[UITestControl.PropertyNames.Name] = sctFileName;
            Mouse.DoubleClick(listItem);
        }

        private static void openBlankWorkSpace()
        {
            //Instance for WinWindow
            almWindow.TechnologyName = "MSAA";
            almWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            if (LibHelper.GetPropertyMenuItem("Modules", Open_Workspace_Calculate.almWindow) == "True")
            {
                //Modules -> Bank ALM
                menuItemModules.SearchProperties[UITestControl.PropertyNames.Name] = "Modules";
                Mouse.Click(menuItemModules);

                menuItem.SearchProperties[UITestControl.PropertyNames.Name] = "Bank ALM";
                menuItem.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(menuItem);
            }
        }
    }
}
