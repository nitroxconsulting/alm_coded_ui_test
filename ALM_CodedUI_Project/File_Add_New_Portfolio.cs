﻿using System;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class File_Add_New_Portfolio : WinWindow
    {

        public static UITestControl menuBar = new WinMenuBar();
        public static UITestControl File = new WinMenuItem(menuBar);
        public static UITestControl NewPortfolio = new WinMenuItem(menuBar);
        public static void VerifyAddedPortfolio()
        {
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";

            UITestControl AsiaTemplate = new WinMenuItem(menuBar);
            UITestControl AgricoleTemplate = new WinMenuItem(menuBar);
            UITestControl EuropeTemplate = new WinMenuItem(menuBar);
            UITestControl FranceTemplate = new WinMenuItem(menuBar);
            UITestControl USTemplate = new WinMenuItem(menuBar);

            OpenPortfolio(AsiaTemplate, "With Asia Template");
            OpenPortfolio(AgricoleTemplate, "With Caisse régionale de Crédit Agricole Template");
            OpenPortfolio(EuropeTemplate, "With Europe Template");
            OpenPortfolio(FranceTemplate, "With France Template");
            OpenPortfolio(USTemplate, "With US Template");
        }
        public static void OpenPortfolio(UITestControl Template, String name)
        {
            File.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            File.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(File);

            NewPortfolio.SearchProperties[UITestControl.PropertyNames.Name] = "New Portfolio";
            File.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(NewPortfolio);

            Template.SearchProperties[UITestControl.PropertyNames.Name] = name;
            Mouse.Click(Template);
            
        }
    }
}
