﻿using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALM_CodedUI_Project
{
     public class Bond : WinWindow
    {
        public static void createBond()
        {
            UITestControl main = new WinWindow();
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            //This is the Main Form Outer Form
            UITestControl treeView = new WinWindow(main);
            treeView.SearchProperties[WinWindow.PropertyNames.ControlName] = "treeView";

            //Portfolio
            WinTreeItem portfolio = new WinTreeItem(treeView);
            portfolio.SearchProperties[WinTreeItem.PropertyNames.Name] = "Portfolio";
            portfolio.SearchProperties["Value"] = "0";

            //Balance Sheet
            WinTreeItem balanceSheet = new WinTreeItem(treeView);
            balanceSheet.SearchProperties[WinTreeItem.PropertyNames.Name] = "Balance Sheet";
            balanceSheet.SearchProperties["Value"] = "1";
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            balanceSheet.SearchConfigurations.Add(SearchConfiguration.NextSibling);

            WinTreeItem landRecievable = new WinTreeItem(treeView);
            landRecievable.SearchProperties[WinTreeItem.PropertyNames.Name] = "loans and receivables to customers";
            landRecievable.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.DoubleClick(landRecievable);

            WinTreeItem corporates = new WinTreeItem(treeView);
            corporates.SearchProperties[WinTreeItem.PropertyNames.Name] = "corporates";
            corporates.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(corporates);

            //Add New Button - Upper Right!
            WinButton addNew = new WinButton(main);
            addNew.SearchProperties[WinButton.PropertyNames.Name] = "Add New";
            Mouse.Click(addNew);

            //Add Products Windows & Select Financial Type
            UITestControl addProductWindow = new UITestControl();
            addProductWindow.TechnologyName = "MSAA";
            addProductWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Add products";
            addProductWindow.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));
            addProductWindow.WindowTitles.Add("Add products");

            WinComboBox rowsToAdd = new WinComboBox(addProductWindow);
            rowsToAdd.SearchProperties[WinComboBox.PropertyNames.Name] = "How many rows do you want to add ";
            rowsToAdd.SelectedItem = "1";

            UITestControl financialType = new UITestControl(addProductWindow);
            financialType.SearchProperties[WinComboBox.PropertyNames.ControlName] = "comboBoxBondFinancialType";

            WinComboBox financialTypeCombo = new WinComboBox(financialType);
            financialTypeCombo.SelectedItem = "Fixed Rate";
            
            WinComboBox nominal = new WinComboBox(addProductWindow);
            nominal.SearchProperties[WinComboBox.PropertyNames.Name] = "Nominal";
            nominal.SelectedItem = "1000";

            WinComboBox coupon = new WinComboBox(addProductWindow);
            coupon.SearchProperties[WinComboBox.PropertyNames.Name] = "Coupon";
            coupon.EditableItem = "0.03";

            WinComboBox duration = new WinComboBox(addProductWindow);
            duration.SearchProperties[WinComboBox.PropertyNames.Name] = "Duration (in months)";
            duration.EditableItem = "60";

            WinComboBox frequency = new WinComboBox(addProductWindow);
            frequency.SearchProperties[WinComboBox.PropertyNames.Name] = "Frequency";
            frequency.EditableItem = "Monthly";

            WinComboBox dateConversion = new WinComboBox(addProductWindow);
            dateConversion.SearchProperties[WinComboBox.PropertyNames.Name] = "Date convention";
            dateConversion.EditableItem = "A30360";

            WinComboBox marketBasis = new WinComboBox(addProductWindow);
            marketBasis.SearchProperties[WinComboBox.PropertyNames.Name] = "Market basis";
            marketBasis.EditableItem = "0";

            WinComboBox accounting = new WinComboBox(addProductWindow);
            accounting.SearchProperties[WinComboBox.PropertyNames.Name] = "Accounting";
            accounting.EditableItem = "LAndR";

            WinComboBox bookPrice = new WinComboBox(addProductWindow);
            bookPrice.SearchProperties[WinComboBox.PropertyNames.Name] = "Book price";
            bookPrice.EditableItem = "100";

            WinButton okButton = new WinButton(addProductWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            WinWindow gridWindow = new WinWindow(main);
            gridWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "detailsGrid";

            WinTable gridTable = new WinTable(gridWindow);
            gridTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";


            WinRowHeader dataRow = new WinRowHeader(gridTable);
            dataRow.SearchProperties[WinRowHeader.PropertyNames.Name] = "Row 0";

            LibHelper.AsssertAreEquals(true, dataRow.Exists);

        }
    }
}
