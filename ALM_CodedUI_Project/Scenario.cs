﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALM_CodedUI_Project
{
    public class Scenario : WinWindow
    {
        public static UITestControl Window = new WinWindow();
        public static UITestControl MainForm = new WinWindow(Window);
        public static UITestControl Tabs = new WinWindow(MainForm);
        public static UITestControl Button = new WinWindow(MainForm);
        public static UITestControl ParametersTab = new WinTabPage(Tabs);
        public static UITestControl AddScenario = new WinButton(Button);
        public static WinWindow createScenario = new WinWindow();
        public static void DeleteScenario()
        {

            openAddScenario();

            UITestControl scenario = new WinWindow();
            scenario.SearchProperties[UITestControl.PropertyNames.Name] = "Scenarios";

            UITestControl listBox = new WinWindow(scenario);
            listBox.SearchProperties[WinWindow.PropertyNames.ControlName] = "availableListBox";

            UITestControl item = new WinListItem(listBox);
            item.SearchProperties[UITestControl.PropertyNames.Name] = "forward inflation + 1%";
            Mouse.Click(item);

            UITestControl delButton = new WinButton(scenario);
            delButton.SearchProperties[UITestControl.PropertyNames.Name] = "Delete";
            Mouse.Click(delButton);

            UITestControl Delscenariowin = new WinWindow();
            Delscenariowin.SearchProperties[UITestControl.PropertyNames.Name] = "Delete scenarios ?";

            UITestControl yesButton = new WinButton(Delscenariowin);
            yesButton.SearchProperties[UITestControl.PropertyNames.Name] = "Yes";
            Mouse.Click(yesButton);

            UITestControl okButton = new WinButton(scenario);
            okButton.SearchProperties[UITestControl.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            UITestControl setupDataGrid = new WinWindow(MainForm);
            setupDataGrid.SearchProperties[WinWindow.PropertyNames.ControlName] = "scenariosSetupDataGridView";

            UITestControl gridTable = new WinTable(setupDataGrid);
            gridTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            UITestControl row = new WinRow(gridTable);
            row.SearchProperties[WinRow.PropertyNames.Value] = "base;CDR_corporate_base;CDR_base;CMS5Y_forward;CODEVI_forward;1;E3M_forward;eonia_forward;gdp_growth_base+5%;0.02;0.006";

            WinCell cell = new WinCell(row);
            cell.SearchProperties[WinCell.PropertyNames.Value] = "CDR_base";

            LibHelper.AsssertAreEquals("CDR_base", cell.Value.ToString());
        }

        public static void openAddScenario() {

            
            Window.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            
            MainForm.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            
            Tabs.SearchProperties[WinWindow.PropertyNames.ControlName] = "mainTabControl";

            
            Button.SearchProperties[WinWindow.PropertyNames.ControlName] = "button1";

            
            ParametersTab.SearchProperties[WinTabPage.PropertyNames.Name] = "Parameters";
            Mouse.Click(ParametersTab);

            
            AddScenario.SearchProperties[WinButton.PropertyNames.Name] = "Add Scenarios ...";
            Mouse.Click(AddScenario);
        }

        public static void openAddVariable()
        {


            Window.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));


            MainForm.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";


            Tabs.SearchProperties[WinWindow.PropertyNames.ControlName] = "mainTabControl";


            Button.SearchProperties[WinWindow.PropertyNames.ControlName] = "button1";


            ParametersTab.SearchProperties[WinTabPage.PropertyNames.Name] = "Parameters";
            Mouse.Click(ParametersTab);


            AddScenario.SearchProperties[WinButton.PropertyNames.Name] = "Add Variables ...";
            Mouse.Click(AddScenario);
        }

        public static void CreatingScenario()
        {
            openAddScenario();

            UITestControl scenario = new WinWindow();
            scenario.SearchProperties[UITestControl.PropertyNames.Name] = "Scenarios";

            UITestControl listBox = new WinWindow(scenario);
            listBox.SearchProperties[WinWindow.PropertyNames.ControlName] = "availableListBox";

            UITestControl addNew = new WinButton(scenario);
            addNew.SearchProperties[WinButton.PropertyNames.Name] = "Add New";
            Mouse.Click(addNew);

            addNewScenario("base");

            WinButton addSelectedScenario = new WinButton(scenario);
            addSelectedScenario.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            Mouse.Click(addSelectedScenario);

            Mouse.Click(addNew);
            addNewScenario("high rates");

            WinButton okButton = new WinButton(scenario);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);


            UITestControl setupDataGrid = new WinWindow(MainForm);
            setupDataGrid.SearchProperties[WinWindow.PropertyNames.ControlName] = "scenariosSetupDataGridView";

            UITestControl gridTable = new WinTable(setupDataGrid);
            gridTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinRow baseDataRow = new WinRow(gridTable);
            baseDataRow.SearchProperties[WinRow.PropertyNames.Name] = "Row 0";

            LibHelper.AsssertAreEquals(true, baseDataRow.Exists);
        }

        public static void addNewScenario(string name)
        {
            createScenario.SearchProperties[WinWindow.PropertyNames.Name] = "Create Scenario";
            createScenario.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            WinEdit scenarioName = new WinEdit(createScenario);
            scenarioName.SearchProperties[WinEdit.PropertyNames.Name] = "Scenario Name";

            scenarioName.Text = name;

            WinButton okButton = new WinButton(createScenario);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);
        }
    }
}
