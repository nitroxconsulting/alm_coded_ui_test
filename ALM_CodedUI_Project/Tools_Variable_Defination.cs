﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class Tools_Variable_Defination
    {
        //Test Data Inputs
        public static string testData1 = "Sample Variable";
        public static string variable1 = "variable_E3M";
        public static string variable2 = "variable_E6M";

        public static void checkVariableDefinations()
        {
            //Opeining Tools in Main Menu
            openToolsMenu();

            //Variable defination Windows Instance
            UITestControl variableDefWindow = new UITestControl();
            variableDefWindow.TechnologyName = "MSAA";
            variableDefWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Variable Definition";
            variableDefWindow.WindowTitles.Add("Variable Definition");

            //Clicking Clone Button
            UITestControl cloneButton = new WinButton(variableDefWindow);
            cloneButton.SearchProperties[WinButton.PropertyNames.Name] = "Clone";
            cloneButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(cloneButton);

            //Instance Window for another dialog Copy Variable
            UITestControl copyVariableWindow = new UITestControl();
            copyVariableWindow.TechnologyName = "MSAA";
            copyVariableWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Copy Variable";
            copyVariableWindow.WindowTitles.Add("Copy Variable");

            //Set Base Variable ComboBox [Need to Instantiate new Window for ois]
            UITestControl copyBaseVarWindow = new WinWindow();
            copyBaseVarWindow.SearchProperties[WinComboBox.PropertyNames.ControlName] = "comboBaseVariable";
            WinComboBox baseVariable = new WinComboBox(copyBaseVarWindow);
             baseVariable.SelectedItem = "ois";

            //Set Variable name
            WinEdit varName = new WinEdit(copyVariableWindow);
            varName.SearchProperties[WinEdit.PropertyNames.Name] = "Variable Name";
            varName.Text = testData1;

            //Click OK Button for Copy Variable window
            WinButton okButton = new WinButton(copyVariableWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            //Click Ok Button for Variable Defination Window
            WinButton okButton2 = new WinButton(variableDefWindow);
            okButton2.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton2);

            openToolsMenu();

            //For Assertion
            WinWindow variablesAdded = new WinWindow();
            variablesAdded.SearchProperties[WinWindow.PropertyNames.ControlName] = "variablesGrid";

            WinTable variablesTable = new WinTable(variablesAdded);
            variablesTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinCell newAddedCell = new WinCell(variablesTable);
            newAddedCell.SearchProperties[WinCell.PropertyNames.Value] = testData1;

            LibHelper.AsssertAreEquals(testData1, newAddedCell.Value);

            Mouse.Click(okButton2);

        }
        public static void openToolsMenu()
        {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl taxAssumptionBut = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            taxAssumptionBut.SearchProperties[UITestControl.PropertyNames.Name] = "Variable Definition ...";
            taxAssumptionBut.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(taxAssumptionBut);

            UITestControl almSolutionDiaglog = new UITestControl();
            almSolutionDiaglog.TechnologyName = "MSAA";
            almSolutionDiaglog.SearchProperties[UITestControl.PropertyNames.Name] = "ALM Solutions";
            almSolutionDiaglog.WindowTitles.Add("ALM Solutions");

            if (almSolutionDiaglog.Exists)
            {
                UITestControl okButton = new WinButton(almSolutionDiaglog);
                okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
                okButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(okButton);
            }
        }

        public static void openToolsMenu_2()
        {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl taxAssumptionBut = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            taxAssumptionBut.SearchProperties[UITestControl.PropertyNames.Name] = "Variable Definition ...";
            taxAssumptionBut.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(taxAssumptionBut);
        }

        public static void createVariable()
        {
            openToolsMenu_2();

            //Variable defination Windows Instance
            UITestControl variableDefWindow = new UITestControl();
            variableDefWindow.TechnologyName = "MSAA";
            variableDefWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Variable Definition";
            variableDefWindow.WindowTitles.Add("Variable Definition");

            UITestControl variableTextBox = new WinWindow(variableDefWindow);
            variableTextBox.SearchProperties[WinWindow.PropertyNames.ControlName] = "newVariableTextBox";

            WinEdit varTextBox = new WinEdit(variableTextBox);
            varTextBox.Text = variable1;

            WinButton add = new WinButton(variableDefWindow);
            add.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            Mouse.Click(add);

            varTextBox.Text = variable2;
            Mouse.Click(add);

            WinButton okButton = new WinButton(variableDefWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            Scenario.openAddVariable();

            UITestControl variableWindow = new WinWindow();
            variableWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Variables";

            WinButton addButton = new WinButton(variableWindow);
            addButton.SearchProperties[WinButton.PropertyNames.Name] = "Add";
            Mouse.Click(addButton);

            WinListItem e6mItem = new WinListItem(variableWindow);
            e6mItem.SearchProperties[WinListItem.PropertyNames.Name] = variable2;
            Mouse.Click(e6mItem);

            Mouse.Click(addButton);

            WinButton okButton2 = new WinButton(variableWindow);
            okButton2.SearchProperties[WinButton.PropertyNames.Name] = "OK";

            Mouse.Click(okButton2);

            UITestControl scenario = new WinWindow(Open_Workspace_Calculate.almWindow);
            scenario.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            UITestControl setupDataGridWindow = new WinWindow(scenario);
            setupDataGridWindow.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            WinTable dataGridViewTable = new WinTable(setupDataGridWindow);
            dataGridViewTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinColumnHeader varE3M = new WinColumnHeader(dataGridViewTable);
            varE3M.SearchProperties[WinColumnHeader.PropertyNames.Name] = variable1;

            WinColumnHeader varE6M = new WinColumnHeader(dataGridViewTable);
            varE6M.SearchProperties[WinColumnHeader.PropertyNames.Name] = variable2;

            LibHelper.AsssertAreEquals(true, varE3M.Exists);
            LibHelper.AsssertAreEquals(true, varE6M.Exists);
        }
    }
}
