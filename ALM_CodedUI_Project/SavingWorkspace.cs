﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    public class SavingWorkspace
    {
        public static void saveWorkspace()
        {
            Mouse.Click(Open_Workspace_Calculate.fileMenu);

            Open_Workspace_Calculate.saveWorkspace.SearchProperties[UITestControl.PropertyNames.Name] = "Save Workspace";
            Mouse.Click(Open_Workspace_Calculate.saveWorkspace);

            //Assertions

            UITestControl main = new UITestControl();
            main.TechnologyName = "MSAA";
            main.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            UITestControl dataGrid = new UITestControl(main);
            dataGrid.SearchProperties[WinWindow.PropertyNames.ControlName] = "resultDataGridView";

            WinTable gridaViewTable = new WinTable(dataGrid);
            gridaViewTable.SearchProperties[WinTable.PropertyNames.Name] = "DataGridView";

            WinRow row = new WinRow(gridaViewTable);
            row.SearchProperties[WinRow.PropertyNames.Value] = "Financial investments;130,000;(null);(null);(null);(null);(null);(null);(null);(n" +
    "ull);(null);(null)";
            row.SearchConfigurations.Add(SearchConfiguration.AlwaysSearch);

            WinCell cell = new WinCell(gridaViewTable);
            cell.SearchProperties[WinCell.PropertyNames.Value] = "Financial investments";

            LibHelper.AsssertAreEquals("True", cell.Exists.ToString());
        }

        public static void saveAs()
        {
            UITestControl menuBar = new WinMenuBar();
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";

            UITestControl File = new WinMenuItem(menuBar);
            File.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            File.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(File);

            UITestControl SaveAs = new WinMenuItem(menuBar);
            SaveAs.SearchProperties[UITestControl.PropertyNames.Name] = "Save WorkSpace as ...";
            SaveAs.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(SaveAs);

            UITestControl saveAsWindow = new UITestControl();
            saveAsWindow.TechnologyName = "MSAA";
            saveAsWindow.SearchProperties[WinWindow.PropertyNames.ClassName] = "#32770";

            WinButton saveButton = new WinButton(saveAsWindow);
            saveButton.SearchProperties[UITestControl.PropertyNames.Name] = "Save";
            saveButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(saveButton);

            deleteFile("New Workspace.alw");
        }
        public static void deleteFile(String filename)
        {
            UITestControl menuBar = new WinMenuBar();
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";

            UITestControl File = new WinMenuItem(menuBar);
            File.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            File.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(File);

            UITestControl SaveAs = new WinMenuItem(menuBar);
            SaveAs.SearchProperties[UITestControl.PropertyNames.Name] = "Open WorkSpace ...";
            SaveAs.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(SaveAs);

            UITestControl saveAsWindow = new UITestControl();
            saveAsWindow.TechnologyName = "MSAA";
            saveAsWindow.SearchProperties[WinWindow.PropertyNames.ClassName] = "#32770";

            UITestControl itemsView = new WinList(saveAsWindow);
            itemsView.TechnologyName = "MSAA";
            itemsView.SearchProperties[WinWindow.PropertyNames.Name] = "Items View";
            UITestControl file = new WinListItem(itemsView);
            file.SearchProperties[WinListItem.PropertyNames.Name] = filename;
            Mouse.Click(file);

            LibHelper.AsssertAreEquals(filename, file.Name);

            Keyboard.SendKeys("^d");
            Keyboard.SendKeys("{Escape}");
        }

        public static void saveTemplate()
        {
            UITestControl menuBar = new WinMenuBar();
            menuBar.TechnologyName = "MSAA";
            menuBar.SearchProperties[UITestControl.PropertyNames.Name] = "menuStrip1";

            UITestControl File = new WinMenuItem(menuBar);
            File.SearchProperties[UITestControl.PropertyNames.Name] = "File";
            File.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(File);

            UITestControl SaveTemplate = new WinMenuItem(menuBar);
            SaveTemplate.SearchProperties[UITestControl.PropertyNames.Name] = "Save as Template";
            SaveTemplate.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(SaveTemplate);

            UITestControl TemplateWindow = new UITestControl();
            TemplateWindow.SearchProperties[WinWindow.PropertyNames.Name] = "Add template";
            TemplateWindow.TechnologyName = "MSAA";
            TemplateWindow.WindowTitles.Add("Add template");

            UITestControl templateNameWindow = new WinWindow(TemplateWindow);
            templateNameWindow.SearchProperties[WinEdit.PropertyNames.ControlName] = "txtName";
            templateNameWindow.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            templateNameWindow.WindowTitles.Add("Add template");

            WinEdit name = new WinEdit(templateNameWindow);
            name.Text = "asdf";

            UITestControl addTemplate = new WinButton(TemplateWindow);
            addTemplate.SearchProperties[WinButton.PropertyNames.Name] = "Add template";
            addTemplate.WindowTitles.Add("Add template");
            Mouse.Click(addTemplate);
            
        }
    }
}
