﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace ALM_CodedUI_Project
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class AlmMain
    {
        public AlmMain()
        {
        }

        public static string alw1 = "maquette V2 sept 2015.alw";
        public static string alw2 = "Maquette V2.2 Crédits.alw";
        public static string alw3 = "Multi CCY.alw";
        public static string alw4 = "Test AFS 1 ligne.alw";
        public static string alw5 = "Test Oli.alw";
        public static string alw6 = "example Dexia.alw";
        public static string alw7 = "Test 1 line model & Roll.alw";
        public static string alw8 = "example Dexia Dividends.alw";
        public static string alw9 = "Low Interest Rate BUG 2.alw";
        public static string alw10 = "KFH model V9.alw";
        public static string alw11 = "demo ebrd (1).alw";
        public static string alw12 = "demo retail bank.alw";
        public static string alw13 = "scenario.alw";
        public static string sct1 = "Sct1 kfh.sct";

        [TestMethod]
        public void OpenCloseWorkspace()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, true, false);
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void File_ClearAll()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, true, false);
            File_ClearAll_Error95.MenuClearAll();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Options_AddCurrency()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, true, false);
            Add_Currency.VerifyAddedCurrency();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Tools_TaxAssumptions()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, true, false);
            Tools_Tax_Assumptions.taxAssumptions();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Tools_VariableDefinations()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            Tools_Variable_Defination.checkVariableDefinations();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Tools_Dividends()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, true);
            Tools_Dividend.testDividend();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Check_OCI()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw12, false, false);
            OCI_BalanceSheet.checkOCI();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Tools_GapAnalysis()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            Tools_Gap_Analysis.gapAnalysis();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void File_Portfolio()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            File_Add_New_Portfolio.VerifyAddedPortfolio();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Options_WorkspaceLogo()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            Add_Image.VerifyAddedImage();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void AddRollSpecValues()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            AddRollSpecValue.VerifyAddRollSpecValue();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Options_CheckBox()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            CheckBox.VerifyCheckBox();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Financial_FixedRate()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            AddFinancialType.VerifyFixedRate();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CommissionAndOthers()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw6, true, false);
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CashAdjustment()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw7, true, false);
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }


        [TestMethod]
        public void SaveWorkspace()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, true, false);
            SavingWorkspace.saveWorkspace();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Cap()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw8, false, false);
            AddFinancialType.VerifyCap();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Swap()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw8, false, false);
            AddFinancialType.VerifySwap();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void VariableRate()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw8, false, false);
            AddFinancialType.VerifyVariableRate();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void ProductSearch()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            QuickProductSearch.VerifyProductSearch();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Forward_Curve_Analyser()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw9, false, false);
            ForwardCurveAnalyser.VerifyForwardCurveAnalyser();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void clearAllCurrency()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw7, false, false);
            File_ClearAll_Error95.ClearAllCurrency();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void AssetTab()
        {                           
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw12, false, false);
            Asset.addAsset();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void VerifyDeleteCur()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw3, false, false);
            Add_Currency.VerifyDeleteCurrency();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void KFHModelV9()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw10, true, false);
            ScenarioValidation.VerifyScennarioValidation();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CopyPasteCategory()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw11, false, false);
            Asset.copyPasteAsset();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void importScenario()
        {
            Open_Workspace_Calculate.ImportScenario(sct1, false);
            Parameters.checkParametersIfEmpty();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void saveAsWorkspace()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, false);
            SavingWorkspace.saveAs();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void dynamicVariables()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw12, false, false);
            Thread.Sleep(2000);
            ToolsMenu.runToolsMenu();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void saveTemplate()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw5, false, true);
            SavingWorkspace.saveTemplate();
            Exit_Bank_Alm.ExitBankAlm();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void DeletesScenario()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw12, false, false);
            Thread.Sleep(1000);
            Scenario.DeleteScenario();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CreateBond()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw13, false, false);
            Thread.Sleep(1000);
            Bond.createBond();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
        }

        [TestMethod]
        public void CreateScenario()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw13, false, false);
            Scenario.CreatingScenario();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CreateVariable()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw13, false, true);
            Scenario.CreatingScenario();
            Tools_Variable_Defination.createVariable();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }

        [TestMethod]
        public void CreateVector()
        {
            Open_Workspace_Calculate.OpenCalculateWorkspace(alw13, false, true);
            Vector.createVector();
            Exit_Bank_Alm.ExitBankAlm();
            SaveChanges.saveChanges();
            Thread.Sleep(1000);
        }
        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
