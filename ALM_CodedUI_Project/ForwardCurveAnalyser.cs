﻿using System;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Drawing;
using System.Windows.Forms;

namespace ALM_CodedUI_Project
{
    class ForwardCurveAnalyser:WinWindow
    {
        public static void VerifyForwardCurveAnalyser()
        {
            
            UITestControl Window = new WinWindow();
            Window.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));

            UITestControl MainForm = new WinWindow(Window);
            MainForm.SearchProperties[WinWindow.PropertyNames.ControlName] = "MainForm";

            UITestControl Tabs = new WinWindow(MainForm);
            Tabs.SearchProperties[WinWindow.PropertyNames.ControlName] = "mainTabControl";

            UITestControl Button = new WinWindow(MainForm);
            Button.SearchProperties[WinWindow.PropertyNames.ControlName] = "button1";
            
            UITestControl ParametersTab = new WinTabPage(Tabs);
            ParametersTab.SearchProperties[WinTabPage.PropertyNames.Name] = "Parameters";
            Mouse.Click(ParametersTab);

            UITestControl ForCurAnalyserButton = new WinButton(Button);
            ForCurAnalyserButton.SearchProperties[WinButton.PropertyNames.Name] = "Forward Curves Analyser";
            Mouse.Click(ForCurAnalyserButton);

            UITestControl ForCurAnalyserWindow = new UITestControl();
            ForCurAnalyserWindow.TechnologyName = "MSAA";
            ForCurAnalyserWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Forward Curves Analyser";

            UITestControl TitleBar = new WinTitleBar(ForCurAnalyserWindow);
            TitleBar.WindowTitles.Add("Forward Curves Analyser");
            Mouse.Click(TitleBar);

            UITestControl CloseButton = new WinButton(TitleBar);
            CloseButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
            CloseButton.WindowTitles.Add("Forward Curves Analyser");
            Mouse.Click(CloseButton);

            //TODO ASSERT
        }
    }
}
