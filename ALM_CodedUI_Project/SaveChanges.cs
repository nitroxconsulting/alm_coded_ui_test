﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    class SaveChanges : WinWindow
    {
        public static void saveChanges() {

            UITestControl almSolutionDiaglog = new UITestControl();
            almSolutionDiaglog.TechnologyName = "MSAA";
            almSolutionDiaglog.SearchProperties[UITestControl.PropertyNames.Name] = "ALM Solutions";
            almSolutionDiaglog.WindowTitles.Add("ALM Solutions");

            if (almSolutionDiaglog.GetProperty("Exists").ToString() == "True")
            {
                UITestControl yesButton = new WinButton(almSolutionDiaglog);
                yesButton.SearchProperties[WinButton.PropertyNames.Name] = "Yes";
                yesButton.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                Mouse.Click(yesButton);
            }
        }
    }
}
