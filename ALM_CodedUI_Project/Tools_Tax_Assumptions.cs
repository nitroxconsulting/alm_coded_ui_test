﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace ALM_CodedUI_Project
{
    class Tools_Tax_Assumptions
    {
        public static void taxAssumptions()
        {
            openToolsMenu();

            UITestControl taxAssumptionWindow = new UITestControl();
            taxAssumptionWindow.TechnologyName = "MSAA";
            taxAssumptionWindow.SearchProperties[UITestControl.PropertyNames.Name] = "Tax Assumptions";
            taxAssumptionWindow.WindowTitles.Add("Tax Assumptions");

            WinComboBox standardRate = new WinComboBox(taxAssumptionWindow);
            standardRate.SearchProperties[WinComboBox.PropertyNames.Name] = "%";

            WinComboBox longTerm = new WinComboBox(taxAssumptionWindow);
            longTerm.SearchProperties[WinComboBox.PropertyNames.Name] = "";

            WinComboBox special = new WinComboBox(taxAssumptionWindow);
            special.SearchProperties[WinComboBox.PropertyNames.Name] = "";
            special.SearchProperties[WinComboBox.PropertyNames.Instance] = "2";

            standardRate.EditableItem = "1.00";
            longTerm.EditableItem = "2.00";
            special.EditableItem = "3.00";

            WinButton okButton = new WinButton(taxAssumptionWindow);
            okButton.SearchProperties[WinButton.PropertyNames.Name] = "OK";
            Mouse.Click(okButton);

            openToolsMenu();

            LibHelper.AsssertAreEquals("1.00", standardRate.SelectedItem.ToString());
            LibHelper.AsssertAreEquals("2.00", longTerm.SelectedItem.ToString());
            LibHelper.AsssertAreEquals("3.00", special.SelectedItem.ToString());

            Mouse.Click(okButton);
        }

        public static void openToolsMenu() {
            Open_Workspace_Calculate.toolsMenu.SearchProperties[UITestControl.PropertyNames.Name] = "Tools";
            Mouse.Click(Open_Workspace_Calculate.toolsMenu);

            UITestControl taxAssumptionBut = new WinMenuItem(Open_Workspace_Calculate.almWindow);
            taxAssumptionBut.SearchProperties[UITestControl.PropertyNames.Name] = "Tax Assumptions";
            taxAssumptionBut.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
            Mouse.Click(taxAssumptionBut);
        }
    }
}
